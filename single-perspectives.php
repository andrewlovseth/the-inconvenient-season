<?php get_header(); ?>

	<?php if ( have_posts() ): ?>

		<?php while ( have_posts() ): the_post(); ?>

			<article class="issue">
				<div class="wrapper">


					<section class="article-header">
						<h2>Perspectives</h2>
					</section>

					<section class="perspectives">
			
				    	<article>
							<?php
								$source = get_field('source', $p->ID);
								$sourceTitle = $source->post_title;
							?>

				    			<?php if(get_field('logo', $source)): ?>
					    			<div class="source-image <?php echo sanitize_title_with_dashes( $source->post_title ); ?>">
					    				<a href="<?php the_field('website', $source); ?>" rel="external">
							    			<img src="<?php $image = get_field('logo', $source); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
							    		</a>
						    		</div>
						    	<?php endif; ?>

							
								<blockquote>
									<?php the_content(); ?>
								</blockquote>

					        <h5>
					        	&mdash; <?php the_field('writer'); ?><?php if ($source): ?><span class="source">, <?php echo $sourceTitle; ?></span><?php endif; ?>
					        </h5>

					        <?php if(get_field('title')): ?>
					        
						        <h6>
						        	<a href="<?php the_field('url'); ?>" rel="external">
						        		<span class="title">“<em><?php the_field('title'); ?></em>”</span>
						        	</a>
						        </h6>

					        <?php elseif(get_field('non_publication_source')): ?>

						        <h6>
						        	<a href="<?php the_field('url'); ?>" rel="external">
						        		<span class="title"><em><?php the_field('non_publication_source'); ?></em></span>
						        	</a>
						        </h6>

					    	<?php endif; ?>



					        <?php 
								$issues = get_posts(array(
									'post_type' => 'post',
									'meta_query' => array(
										array(
											'key' => 'perspectives',
											'value' => '"' . get_the_ID() . '"',
											'compare' => 'LIKE'
										)
									)
								));
								if( $issues ): ?>

								<?php foreach( $issues as $issue ): ?>
								
									<h4 class="edition"><a href="<?php echo get_permalink( $issue->ID ); ?>">from the <strong><?php echo get_the_title( $issue->ID ); ?></strong> edition</a></h4>

								<?php endforeach; ?>
							<?php endif; ?>

							<div class="tags">
								<?php the_tags( '', ', ', ' ' ); ?>
							</div>


						</article>


					</section>





				</div>
			</article>

		<?php endwhile; ?>

	<?php endif; ?>


<?php get_footer(); ?>