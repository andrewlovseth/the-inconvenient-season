<html>
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width" />

	<?php get_template_part('partials/global/meta-tags'); ?>

	<link rel="stylesheet" type="text/css" href="https://cloud.typography.com/6104232/742388/css/fonts.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('stylesheet_url'); ?>" />
	<link rel="shortcut icon" href="<?php bloginfo('template_directory'); ?>/images/favicon.png" />

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?> id="<?php echo the_slug();?>">

	<section id="explore">
		<div class="wrapper">

			<span>Explore:</span>

			<?php $terms = get_field('explore_tags', 'options'); if( $terms ): ?>

				<?php foreach( $terms as $term ): ?>

					<a href="<?php echo get_term_link( $term ); ?>"><?php echo $term->name; ?></a>

				<?php endforeach; ?>

			<?php endif; ?>


		</div>
	</section>

	<header>

		<h1>
			<a href="<?php echo site_url('/'); ?>" id="logo">
				<img src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</h1>
		
		<a href="#" id="toggle">
			<div class="patty"></div>
		</a>

	</header>

	<nav>
		<div class="wrapper">

			<ul>
				<li><a href="<?php echo site_url('/'); ?>">Home</a></li>
				<li><a href="<?php echo site_url('/about/'); ?>">About</a></li>
				<li><a href="<?php echo site_url('/archives/'); ?>">Archives</a></li>
				<li><a href="<?php echo site_url('/contact/'); ?>">Contact</a></li>
			</ul>

		</div>
	</nav>