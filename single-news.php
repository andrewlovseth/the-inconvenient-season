<?php get_header(); ?>

	<?php if ( have_posts() ): ?>

		<?php while ( have_posts() ): the_post(); ?>

			<article class="issue">
				<div class="wrapper">


					<section class="article-header">
						<h2>News</h2>
					</section>


					<section class="news">

			    		<article class="primary">
			    			<?php $source = get_field('source'); ?>

			    			<?php if(get_field('logo', $source->ID)): ?>
				    			<div class="source-image <?php echo sanitize_title_with_dashes( $source->post_title ); ?>">
				    				<a href="<?php the_field('website', $source->ID); ?>" rel="external">
						    			<img src="<?php $image = get_field('logo', $source->ID); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
						    		</a>
					    		</div>
					    	<?php endif; ?>

					        <h3><a href="<?php the_field('url'); ?>" rel="external"><?php the_title(); ?></a></h3>

					        <?php 
								$issues = get_posts(array(
									'post_type' => 'post',
									'meta_query' => array(
										array(
											'key' => 'news',
											'value' => '"' . get_the_ID() . '"',
											'compare' => 'LIKE'
										)
									)
								));
								if( $issues ): ?>

								<?php foreach( $issues as $issue ): ?>
								
									<h4 class="edition"><a href="<?php echo get_permalink( $issue->ID ); ?>">from the <strong><?php echo get_the_title( $issue->ID ); ?></strong> edition</a></h4>

								<?php endforeach; ?>
							<?php endif; ?>

							<div class="tags">
								<?php the_tags( '', ', ', ' ' ); ?>
							</div>

			    		</article>

			    	</section>


				</div>
			</article>

		<?php endwhile; ?>

	<?php endif; ?>


<?php get_footer(); ?>