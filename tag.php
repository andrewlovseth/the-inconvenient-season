<?php get_header(); ?>

	<section id="tag-timeline">
		<div class="wrapper">


			<section class="article-header">
				<h2><?php single_tag_title(); ?></h2>
			</section>


			<section id="items">

				<?php
					$term = get_queried_object();
					$tag_id = $term->term_id;
					$args = array(
						'post_type' => array( 'news', 'perspectives' ),
						'posts_per_page' => 500,
						'tag_id' => $tag_id
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

					<section class="item-block">
						<aside>

							<span><?php the_time('m/d/y'); ?></span>

						</aside>
						<?php if ( get_post_type( get_the_ID() ) == 'news' ): ?>

					    	<article class="news">
				    			<?php
				    				$source = get_field('source', $p->ID);
				    				$sourceTitle = $source->post_title;
				    			?>

				    			<h6><?php echo $sourceTitle; ?></h6>

						        <h5>
						        	<a href="<?php the_field('url', $p->ID); ?>" rel="external">
						        		<?php echo get_the_title($p->ID); ?>
						        	</a>
						        </h5>
				    		</article>

						<?php endif; ?>


						<?php if ( get_post_type( get_the_ID() ) == 'perspectives' ): ?>

					    	<article class="perspective">
								<?php
									$source = get_field('source', $p->ID);
									$sourceTitle = $source->post_title;
								?>
								
									<blockquote>
										<?php the_content(); ?>
									</blockquote>

						        <h5>
						        	&mdash; <?php the_field('writer', $p->ID); ?><?php if ($source): ?><span class="source">, <?php echo $sourceTitle; ?></span><?php endif; ?>
						        </h5>

						        <?php if(get_field('title', $p->ID)): ?>
						        
							        <h6>
							        	<a href="<?php the_field('url', $p->ID); ?>" rel="external">
							        		<span class="title">“<em><?php the_field('title', $p->ID); ?></em>”</span>
							        	</a>
							        </h6>

						        <?php elseif(get_field('non_publication_source', $p->ID)): ?>

							        <h6>
							        	<a href="<?php the_field('url', $p->ID); ?>" rel="external">
							        		<span class="title"><em><?php the_field('non_publication_source', $p->ID); ?></em></span>
							        	</a>
							        </h6>

						    	<?php endif; ?>

							</article>

						<?php endif; ?>



					</section>



				<?php endwhile; endif; wp_reset_postdata(); ?>


			</section>


		</div>
	</section>

<?php get_footer(); ?>