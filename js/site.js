$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});


	$('#toggle').click(function(){

		$('header').toggleClass('open');
		$('nav').fadeToggle(300);

		return false;

	});
    
});