<?php get_header(); ?>

<?php if ( have_posts() ): ?>

	<?php while ( have_posts() ): the_post(); ?>

		<article class="issue">
			<div class="wrapper">

				<section class="article-header">
					<h2><?php the_title(); ?></h2>
				</section>

				<section class="article-body">
					<?php the_content(); ?>
				</section>

			</div>
		</article>

	<?php endwhile; ?>

<?php endif; ?>

<?php get_footer(); ?>