<?php get_header(); ?>



		<article class="issue">
			<div class="wrapper">

				<section class="book-header">
					<h2>Books</h2>
				</section>

				<section class="book-list">

<?php if ( have_posts() ): ?>

	<?php while ( have_posts() ): the_post(); ?>


					<div class="book">
						

						<div class="cover">
							<a href="<?php the_field('amazon_link'); ?>?tag=andrewlovseth-20" rel="external">
								<img src="<?php $image = get_field('cover'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</a>
						</div>

						<div class="info">
							<?php $genres = wp_get_post_terms($post->ID, 'genre'); ?>
							<?php foreach($genres as $genre): ?>
								<span class="genre"><?php echo $genre->name; ?></span>
							<?php endforeach; ?>
							
							<h2>
								<a href="<?php the_field('amazon_link'); ?>?tag=andrewlovseth-20" rel="external"><?php the_title(); ?></a>
							</h2>
							<h3><?php the_field('subtitle'); ?></h3>
							<h4>by <?php the_field('author'); ?></h4>

						</div>

					</div>
	<?php endwhile; ?>

<?php endif; ?>




				</section>

			</div>
		</article>



<?php get_footer(); ?>