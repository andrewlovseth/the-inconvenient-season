<?php get_header(); ?>

	<article class="issue">
		<div class="wrapper">

			<section class="article-header">
				<h2>Tags</h2>
			</section>

		</div>
	</article>


	<section id="tag-list">
		<div class="wrapper">

			<?php 
				$args = array(
					'orderby' => 'count',
					'order' => DESC,
					'hide_empty' => true,
					'number' => 40
				);
				$tags = get_tags($args);
				foreach ( $tags as $tag ): ?>

				<a href="<?php echo get_tag_link( $tag->term_id ); ?>"><?php echo $tag->name; ?> (<?php echo $tag->count; ?>)</a>

			<?php endforeach; ?>


		</div>
	</section>


	<article class="issue">
		<div class="wrapper">

			<section class="article-header">
				<h2>Archives</h2>
			</section>

		</div>
	</article>


	<section id="calendar">
		<div class="wrapper">


			<?php

			    $years = $wpdb->get_results(
			        "SELECT DISTINCT YEAR(post_date) AS year 
			         FROM $wpdb->posts 
			         WHERE post_status = 'publish' 
			         AND post_type = 'post'
			         ORDER BY post_date 
			         DESC
			    ");

			    if($years): ?>

			     	<?php $months = array(12 => "December", 11 => "November", 10 => "October", 9 => "September", 8 => "August", 7 => "July", 6 => "June", 5 => "May", 4 => "April", 3 => "March",  2 => "February", 1 => "January"); ?>

			        <?php foreach($years as $year): ?>

			            <?php foreach($months as $month_num => $month): ?>

			               <?php if( $search_month = $wpdb->query(

		                       "SELECT MONTHNAME(post_date) as month 
		                        FROM $wpdb->posts  
		                        WHERE MONTHNAME(post_date) = '$month'
		                        AND YEAR(post_date) = $year->year 
		                        AND post_type = 'post'
		                        AND post_status = 'publish'
		                        ORDER BY post_date 
		                        DESC

			               	   ")): ?>

			                    <?php
			                        $timeForFirstDayOfMonth = mktime(0, 0, 0, $month_num, 1, $year->year);
			                        $monthSlug = sanitize_title_with_dashes(date('M', $timeForFirstDayOfMonth));
			                    ?>

								<table cellspacing="0" class="calendar calendar-container <?php echo $monthSlug; ?>">


								    <caption class="month-year-caption">
								        <span class="monthName"><?php echo date('M Y', $timeForFirstDayOfMonth); ?></span>
								    </caption>

								    <thead>
								        <tr>
								        	<?php
								        		$weekdays = array('S', 'M', 'T', 'W', 'T', 'F', 'S');
								        		$firstDayOfWeek = 0;
								        		for ($counter = 0, $i = $firstDayOfWeek; 7 > $counter; $counter++, $i++): ?>
								        		
								        			<th><?php echo $weekdays[$i]; ?></th>

								        			<?php if (6 == $i) { $i = -1; } ?>

								        	<?php endfor; ?>
								        </tr>
								    </thead>


								    <tbody>
								        <tr>
											<?php
											    // Total number of days in current month/year
											    $totalDaysInMonth = date('t', $timeForFirstDayOfMonth);

											    // Weekday for first day of current month/year
											    $weekdayForFirstDayOfMonth = date('w', $timeForFirstDayOfMonth);
											?>

											<?php
											    // If 'first day of week' is not equal to weekday for first day of month then proceed further to output empty TDs
											    if ($firstDayOfWeek != $weekdayForFirstDayOfMonth): ?>
											        
											        <?php 
											        	// Calculate total empty days
											        	$totalEmptyDays = ($weekdayForFirstDayOfMonth - $firstDayOfWeek);
											        ?>

											    
											        <?php
												        // If first day of week is greater than weekday for first day of month then add 7 days to total empty days
											        	if ($firstDayOfWeek > $weekdayForFirstDayOfMonth): ?>
											        
											        	<?php $totalEmptyDays += 7; ?>
											        <?php endif; ?>

											        <?php 
											        	// Loop for 'total empty days' to output empty TDs if first day of current month/year doesn't start on 'first day of week'
											        	for ($i = 0; $i < $totalEmptyDays; $i++): ?>

											       		<td>&nbsp;</td>

											    	<?php endfor; ?>
											        
											<?php endif; ?>
											            
											<?php
											    // Loop for total number of days in current month/year to output calendar with posts
											    for ($day = 1; $day <= $totalDaysInMonth; $day++): ?>
											    
											    <?php
											        // If new week started then close current table row and start new one
											        if (1 < $day && $firstDayOfWeek == date('w', mktime(0, 0, 0, $month_num, $day, $year->year))): ?>
			
												        </tr>
												        <tr>

												<?php endif; ?>

												<td>
													<?php

														$args = array(
															'post_type' => 'post',
															'posts_per_page' => 1,
															'date_query' => array(
																array(
																	'year'  => $year->year,
																	'month' => $month_num,
																	'day'   => $day,
																),
															),
														);
														$query = new WP_Query( $args );
														if ( $query->have_posts() ): ?>

														<?php while ( $query->have_posts() ) : $query->the_post(); ?>

															<a href="<?php the_permalink(); ?>"><?php echo $day; ?></a>

														<?php endwhile; ?>

													<?php else: ?>

														<span><?php echo $day; ?></span>

													<?php endif; wp_reset_postdata(); ?>

											    </td>
												
											<?php endfor;?>

										    <?php 
											    // Weekday for last day of current month/year
											    $weekdayForLastDayOfMonth = date('w', mktime(0, 0, 0, $month_num, $totalDaysInMonth, $year->year));

											    // Calculate total empty days
											    $totalEmptyDays = ($firstDayOfWeek - $weekdayForLastDayOfMonth - 1);
											?>

											<?php 
												// If first day of week is less than or equals to weekday for last day of month then add 7 days to total empty days
										    	if ($firstDayOfWeek <= $weekdayForLastDayOfMonth) {
										        	$totalEmptyDays += 7;
										    	}
										    ?>

										    <?php 
											    // Loop for 'total empty days' to output empty TDs if last day of current month/year doesn't end on 'first day of week'
											    for ($i = 0; $i < $totalEmptyDays; $i++): ?>

											    	<td>&nbsp;</td>
											<?php endfor; ?>

								        </tr>

								    </tbody>
								</table>

			                <?php endif; ?>

		            	<?php endforeach; ?>

			      <?php endforeach; ?>

			<?php endif; ?>

		</div>
	</section>
	   	
<?php get_footer(); ?>