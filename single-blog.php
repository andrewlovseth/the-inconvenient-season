<?php get_header(); ?>


	<section class="blog-header">
		<div class="wrapper">


			<div class="heading">
				<h1><strong>Blog:</strong> The American Spring</h1>
			</div>

		</div>
	</section>

	

	<?php if ( have_posts() ): ?>

		<section class="blog-entries">
			<div class="wrapper">

				<?php while ( have_posts() ): the_post(); ?>

					<article class="blog">

						<div class="article-header">
							<span class="date"><?php the_time('l, F jS, Y'); ?></span>
							<h2 class="title"><?php the_title(); ?></h2>
						</div>

						<div class="article-body">
							<?php the_content(); ?>
						</div>

					</article>

				<?php endwhile; ?>

			</div>
		</section>

	<?php endif; ?>


<?php get_footer(); ?>