<?php



/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

show_admin_bar(false);


function set_posts_per_page_for_books_cpt( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'books' ) ) {
    $query->set( 'posts_per_page', '50' );
  }
}
add_action( 'pre_get_posts', 'set_posts_per_page_for_books_cpt' );



/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );





/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/


/* The Slug */
function the_slug() {
	$post_data = get_post($post->ID, ARRAY_A);
	$slug = $post_data['post_name'];
	return $slug;
}



/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');



/* List of Tag Slugs */
function entry_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo $tag->slug; 
	  }
	}
}

function remove_menus(){
  remove_menu_page( 'edit-comments.php' );
}
add_action( 'admin_menu', 'remove_menus' );


// First, create a function that includes the path to your favicon
function add_favicon() {
  	$favicon_url = get_stylesheet_directory_uri() . '/images/wordpress-favicon.ico';
	echo '<link rel="shortcut icon" href="' . $favicon_url . '" />';
}
  
// Now, just make sure that function runs when you're on the login page and admin pages  
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');



/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/



function my_relationship_query( $args, $field, $post_id ) {

    $args['orderby'] = 'date';
    $args['order'] = 'DESC';

    return $args;
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);


if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');








/*

    ----------------------------------------------------------------------
    					XX MailChimp Template
    ----------------------------------------------------------------------

*/

function filter_the_content($content) {
	apply_filters('the_content', $content );
	return $content;
}

function fields_in_feed($content) {  
    if(is_feed()) {  
        $post_id = get_the_ID();

        // Header
        $output = '<table class="header" style="width: 100%;">';  
        	$output .= '<tr>';
        		$output .= '<td style="border-top: 1px solid #4a4a4a; border-bottom: 1px solid #4a4a4a; padding: 10px 0;">';
	        		$output .= '<h2 style="text-transform: uppercase; font-size: 11px !important; font-weight: bold; letter-spacing: 2px; text-align: center;"><a style="font-weight: bold;" href="' . get_permalink() . '">' . get_the_title() . '</a></h2>';
        		$output .= '</td>';
        	$output .= '</tr>';
        $output .= '</table>';  


        // News
        $news = get_post_meta($post_id, 'news', true);
        if( $news ) {
        	$output .= '<table class="news">';  

        	$counter = 0;
        	foreach( $news as $n) {
	        	$counter++;

	        	if($counter == 1) {

	        		$source = get_post_meta($n, 'source', true);
					$output .= '<tr class="article primary">';
						$output .= '<td style="padding: 30px 0 50px 0;">';
							$output .= '<h4 class="source" style="text-transform: uppercase; letter-spacing: 1px; font-size: 10px !important;">'. get_the_title($source) . '</h4>';
							$output .= '<h3 class="article-title" style="font-size: 32px !important; line-height: 36px !important;"><a href="'. get_post_meta($n, 'url', true) .'">' . get_the_title($n) . '</a></h3>';
						$output .= '</td>';
					$output .= '</tr>';

	        	} elseif($counter > 1 && $counter < 4) {

	        		$source = get_post_meta($n, 'source', true);
					$output .= '<tr class="article secondary">';
						$output .= '<td style="padding: 0 0 50px 0;">';
							$output .= '<h4 class="source" style="text-transform: uppercase; letter-spacing: 1px; font-size: 10px !important;">'. get_the_title($source) . '</h4>';
							$output .= '<h3 class="article-title" style="font-size: 24px !important;"><a href="'. get_post_meta($n, 'url', true) .'">' . get_the_title($n) . '</a></h3>';
						$output .= '</td>';
					$output .= '</tr>';

	        	} else {

	        		$source = get_post_meta($n, 'source', true);
					$output .= '<tr class="article tertiary">';
						$output .= '<td style="padding: 0 0 30px 0;">';
							$output .= '<h4 class="source" style="text-transform: uppercase; letter-spacing:1px; font-size: 10px !important;">'. get_the_title($source) . '</h4>';
							$output .= '<h3 class="article-title" style="font-size: 20px !important;"><a href="'. get_post_meta($n, 'url', true) .'">' . get_the_title($n) . '</a></h3>';
						$output .= '</td>';
					$output .= '</tr>';
	        	}

        	}

        	$output .= '</table>';
 		}


        // Perspectives
        $perspectives = get_post_meta($post_id, 'perspectives', true);


        if( $perspectives ) {
        	$output .= '<table class="perspectives">';  

	        	$output .= '<tr>';
	        		$output .= '<td style="padding: 50px 0 20px 0;">';
		        		$output .= '<h2 style="text-transform: uppercase; font-size: 11px !important; font-weight: bold; letter-spacing: 2px; ">Perspectives</h2>';
	        		$output .= '</td>';
	        	$output .= '</tr>';

	        	foreach( $perspectives as $per) {

	        		$source = get_post_meta($per, 'source', true);
	        		$sourceTitle = get_post_field('post_title', $source, 'display');

	        		$perspectiveContent = get_post_field('post_content', $per, 'raw');

					$output .= '<tr class="perspective">';
						$output .= '<td style="padding: 0 0 50px 0;">';
							$output .= '<blockquote style="font-size: 16px; padding: 0 0 0 30px; margin: 0;">';
								$output .= $perspectiveContent;
							$output .= '</blockquote>';
							
							$output .= '<h5 style="font-size: 14px !important; padding: 10px 0 0 30px; margin: 0; font-family: Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif;">&mdash; ' . get_post_meta($per, 'writer', true);
								if($sourceTitle) { 
									$output .= '<span class="source">, ' . $sourceTitle . '</span>';
								}
							$output .= '</h5>';

		       				if(get_post_meta($per, 'title', true)) {
		       					$output .= '<h6 style="font-size: 12px !important; padding: 0 0 0 30px; font-family: Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif;"><a href="' . get_post_meta($per, 'url', true) . '"><span class="title">“' . get_post_meta($per, 'title', true) . '”</span>';
		       				} elseif( get_post_meta($per, 'non_publication_source', true) ) {
		       					$output .= '<h6 style="font-size: 12px !important; padding: 0 0 0 30px; font-family: Source Sans Pro, Helvetica Neue, Helvetica, Arial, sans-serif;"><a href="' . get_post_meta($per, 'url', true) . '"><span class="title">' . get_post_meta($per, 'non_publication_source', true) . '</span>';
		       				}

						$output .= '</td>';
					$output .= '</tr>';

	        	}

        	$output .= '</table>';
 		}

        $content = $content.$output;  
    }  
    return $content;  
}  
add_filter('the_content','fields_in_feed');
