<?php $news = get_field('news'); if( $news ): ?>

	<section class="news">

	    <?php $counter = 0; foreach( $news as $p): ?>
	    	<?php $counter++; ?>

	    	<?php if($counter == 1): ?>

	    		<article class="news primary">
	    			<?php $source = get_field('source', $p->ID); ?>

	    			<div class="source-image <?php echo sanitize_title_with_dashes( $source->post_title ); ?>">
		    			<img src="<?php $image = get_field('logo', $source->ID); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
		    		</div>

			        <h3>
			        	<a href="<?php the_field('url', $p->ID); ?>" rel="external"><?php echo get_the_title($p->ID); ?></a>
			        </h3>
	    		</article>

		    <?php elseif($counter > 1 && $counter < 4): ?>

	    		<article class="news secondary<?php if($counter == 3): ?> last<?php endif;?>">
	    			<?php
	    				$source = get_field('source', $p->ID);
	    				$sourceTitle = $source->post_title;
	    			?>

	    			<h6><?php echo $sourceTitle; ?></h6>

			        <h4>
			        	<a href="<?php the_field('url', $p->ID); ?>" rel="external">
			        		<?php echo get_the_title($p->ID); ?>
			        	</a>
			        </h4>
	    		</article>

			<?php else: ?>

		    	<article class="news tertiary">
	    			<?php
	    				$source = get_field('source', $p->ID);
	    				$sourceTitle = $source->post_title;
	    			?>

	    			<h6><?php echo $sourceTitle; ?></h6>

			        <h5>
			        	<a href="<?php the_field('url', $p->ID); ?>" rel="external">
			        		<?php echo get_the_title($p->ID); ?>
			        	</a>
			        </h5>
	    		</article>

			<?php endif; ?>

	    <?php endforeach; ?>
	</section>

<?php wp_reset_postdata(); endif; ?>