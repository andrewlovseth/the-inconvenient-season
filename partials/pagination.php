<section id="pagination">
	<div class="wrapper">

		<?php $currentPost = get_post($post->ID); ?>

		<?php $prev_post = get_adjacent_post(false, '', true); if(!empty($prev_post)): ?>

			<div class="prev">
			  <a href="<?php echo get_permalink($prev_post->ID); ?>"><span class="arrow">&larr;</span> <span class="caption">Day Before</span></a>
			</div>

		<?php endif ?>

		<?php
			$currentPostDate = get_the_time('Ymd', $currentPost->ID);
		    $lastestPost = get_posts( 'numberposts=1' );
			$lastestPostDate = get_the_time('Ymd', $lastestPost[0]->ID);
			$secondLastestPostDate = strtotime('-1 day', strtotime($lastestPostDate));
			$currentPostDateTime = strtotime($currentPostDate);

			if($currentPostDate == $lastestPostDate) {
				$prevPostCount = 4;
			} elseif ($currentPostDateTime == $secondLastestPostDate) {
				$prevPostCount = 3;
			} else {
				$prevPostCount = 2;
			}

			$prevArgs = array(
				'post_type' => 'post',
				'posts_per_page' => $prevPostCount,
				'order' => 'DESC',
				'post_status' => 'publish',
				'date_query'    => array(
					'column'  => 'post_date',
					'before'   => get_the_date()
				)
			);
			$prevQuery = new WP_Query( $prevArgs );
	        $array_rev = array_reverse($prevQuery->posts);
	        $prevQuery->posts = $array_rev;

			if ( $prevQuery->have_posts() ) : while ( $prevQuery->have_posts() ) : $prevQuery->the_post(); ?>

			<div class="day past">
				<a href="<?php the_permalink(); ?>">
					<span><?php the_time('D'); ?></span>
					<?php the_time('j'); ?>
				</a>
			</div>

		<?php endwhile; endif; wp_reset_postdata(); ?>

			<div class="day active">
				<a href="<?php echo esc_url( get_permalink($currentPost->ID) ); ?>">
					<span><?php echo get_the_time('D', $currentPost->ID); ?></span>
					<?php echo get_the_time('j', $currentPost->ID); ?>
				</a>
			</div>


		<?php
			$currentPostDate = get_the_time('Ymd', $currentPost->ID);
		    $firstPost = get_posts( 'numberposts=1&order=ASC' );
			$firstPostDate = get_the_time('Ymd', $firstPost[0]->ID);

			if($currentPostDate == $firstPostDate) {
				$nextPostCount = 4;
			} elseif ($currentPostDate == $firstPostDate + 1) {
				$nextPostCount = 3;
			} else {
				$nextPostCount = 2;
			}

			$nextArgs = array(
				'post_type' => 'post',
				'posts_per_page' => $nextPostCount,
				'offset' => 1,
				'order' => 'ASC',
				'post_status' => 'publish',
				'date_query' => array(
					'column' => 'post_date',
					'after' => get_the_date('F j, Y', $currentPost->ID),
					'inclusive' => false
				)
			);
			$nextQuery = new WP_Query( $nextArgs );

			if ( $nextQuery->have_posts() ) : while ( $nextQuery->have_posts() ) : $nextQuery->the_post(); ?>

			<div class="day future">
				<a href="<?php the_permalink(); ?>">
					<span><?php the_time('D'); ?></span>
					<?php the_time('j'); ?>
				</a>
			</div>

		<?php endwhile; endif; wp_reset_postdata(); ?>

		<?php $next_post = get_adjacent_post(false, '', false); if(!empty($next_post)): ?>

			<div class="next">
			  <a href="<?php echo get_permalink($next_post->ID); ?>"><span class="caption">Day After</span> <span class="arrow">&rarr;</span></a>
			</div>

		<?php endif ?>


	</div>
</section>