<?php if(is_front_page()): ?>

	<title><?php the_field('home_meta_title', 'options'); ?></title>
	<meta name="description" content="<?php the_field('home_meta_description', 'options'); ?>" />
	<meta property="og:description" content="<?php the_field('home_meta_description', 'options'); ?>" />
	<meta property="og:image" content="<?php $image = get_field('home_facebook_image', 'options'); echo $image['url']; ?>" />


<?php elseif(is_tag()): ?>

	<title><?php single_tag_title(); ?> – The Inconvenient Season</title>
	<meta name="description" content="<?php the_field('home_meta_description', 'options'); ?>" />
	<meta property="og:description" content="<?php the_field('home_meta_description', 'options'); ?>" />
	<meta property="og:image" content="<?php $image = get_field('home_facebook_image', 'options'); echo $image['url']; ?>" />

<?php elseif(is_post_type_archive('blog')): ?>

	<title>Blog – The Inconvenient Season</title>
	<meta name="description" content="<?php the_field('home_meta_description', 'options'); ?>" />
	<meta property="og:description" content="<?php the_field('home_meta_description', 'options'); ?>" />
	<meta property="og:image" content="<?php $image = get_field('home_facebook_image', 'options'); echo $image['url']; ?>" />


<?php else: ?>

	<?php $postID = get_queried_object_id(); ?>

	<?php if(get_field('meta_title', $postID)): ?>
		<title><?php the_field('meta_title', $postID); ?> – The Inconvenient Season</title>
	<?php else: ?>
		<title><?php echo get_the_title($postID); ?> – The Inconvenient Season</title>
	<?php endif; ?>

	<?php if(get_field('meta_description', $postID)): ?>
		<meta name="description" content="<?php the_field('meta_description', $postID); ?>" />
		<meta property="og:description" content="<?php the_field('meta_description', $postID); ?>" />
		<meta property="og:image" content="<?php $image = get_field('home_facebook_image', 'options'); echo $image['url']; ?>" />


	<?php endif; ?>

<?php endif; ?>