<?php $perspectives = get_field('perspectives'); if( $perspectives ): ?>

	<section class="perspectives">
		
		<h2 class="section-title">Perspectives</h2>

	    <?php foreach( $perspectives as $p): ?>

	    	<article class="perspective">
				<?php
					$source = get_field('source', $p->ID);
					$sourceTitle = $source->post_title;
				?>
				
					<blockquote>
						<?php
							$content = $p->post_content;
							$content = apply_filters( 'the_content', $content );
							echo $content;
						?>
					</blockquote>

		        <h5>
		        	&mdash; <?php the_field('writer', $p->ID); ?><?php if ($source): ?><span class="source">, <?php echo $sourceTitle; ?></span><?php endif; ?>
		        </h5>

		        <?php if(get_field('title', $p->ID)): ?>
		        
			        <h6>
			        	<a href="<?php the_field('url', $p->ID); ?>" rel="external">
			        		<span class="title">“<em><?php the_field('title', $p->ID); ?></em>”</span>
			        	</a>
			        </h6>

		        <?php elseif(get_field('non_publication_source', $p->ID)): ?>

			        <h6>
			        	<a href="<?php the_field('url', $p->ID); ?>" rel="external">
			        		<span class="title"><em><?php the_field('non_publication_source', $p->ID); ?></em></span>
			        	</a>
			        </h6>

		    	<?php endif; ?>

			</article>

	    <?php endforeach; ?>

	</section>

<?php wp_reset_postdata(); endif; ?>